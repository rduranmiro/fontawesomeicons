package es.applogical.fontawesome;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.javafx.FontIcon;

import java.util.Iterator;

/**
 * @author RamonDuran
 */
public class FontAwesomeIcons extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        VBox root = new VBox();

        AnchorPane anchorPane = new AnchorPane();
        anchorPane.prefHeightProperty().bind(root.heightProperty());
        anchorPane.prefWidthProperty().bind(root.widthProperty());

        Button btn1 = new Button("Test 1");
        btn1.setPrefWidth(120);
        btn1.setGraphic(new FontIcon(FontAwesome.COFFEE));
        btn1.setGraphicTextGap(30);
        btn1.setOnAction(e -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Font Awesome Sample");
            alert.setContentText("Button 1 clicked");
            alert.showAndWait();
        });

        Button btn2 = new Button("Test 2");
        btn2.setPrefWidth(120);
        btn2.setGraphic(new FontIcon(FontAwesome.ADDRESS_BOOK));
        btn2.setGraphicTextGap(30);
        btn2.setOnAction(e -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Font Awesome Sample");
            alert.setContentText("Button 2 clicked");
            alert.showAndWait();
        });

        TextField txtFilter = new TextField();
        txtFilter.setPrefWidth(200);
        txtFilter.setPromptText("Search");

        TableView table = new TableView();
        TableColumn col1 = new TableColumn("Icon");
        col1.setStyle("-fx-alignment: center; -fx-font-size: 14px;");

        TableColumn col2 = new TableColumn("Name Icon");
        TableColumn col3 = new TableColumn("Description");

        table.getColumns().addAll(col1, col2, col3);
        table.setPrefWidth(572);

        MenuItem item = new MenuItem("Copy");
        item.setOnAction((ActionEvent event) -> {
            ObservableList rowList = table.getSelectionModel().getSelectedItems();
            StringBuilder clipboardString = new StringBuilder();

            Iterator it = rowList.iterator();
            while (it.hasNext()) {
                FontAwesomeItem i = (FontAwesomeItem) it.next();
                clipboardString.append(i.getIconName());
            }
            final ClipboardContent content = new ClipboardContent();

            content.putString(clipboardString.toString());
            Clipboard.getSystemClipboard().setContent(content);
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().add(item);
        table.setContextMenu(menu);

        FontAwesome[] fonts = FontAwesome.values();
        ObservableList<FontAwesomeItem> list = FXCollections.observableArrayList();
        FilteredList<FontAwesomeItem> filteredList = new FilteredList<>(list);

        txtFilter.textProperty().addListener((observable, oldValue, newValue) -> {
            txtFilter.setText(txtFilter.getText().toUpperCase());
            txtFilter.positionCaret(txtFilter.getLength());
            filteredList.setPredicate(s -> s.getIconName().contains(txtFilter.getText()));
            table.setItems(filteredList);
            table.refresh();
        });

        col1.setCellValueFactory(new PropertyValueFactory<>("icon"));
        col2.setCellValueFactory(new PropertyValueFactory<>("iconName"));
        col3.setCellValueFactory(new PropertyValueFactory<>("description"));

        for (FontAwesome font : fonts) {
            list.add(new FontAwesomeItem(font.name(), new FontIcon(FontAwesome.valueOf(font.name())), font.getDescription()));
        }

        table.getItems().addAll(list);
        table.refresh();

        anchorPane.getChildren().addAll(btn1, btn2, table, txtFilter);
        AnchorPane.setLeftAnchor(btn1, 14.0);
        AnchorPane.setTopAnchor(btn1, 10.0);

        AnchorPane.setLeftAnchor(btn2, 160.0);
        AnchorPane.setTopAnchor(btn2, 10.0);

        AnchorPane.setLeftAnchor(table, 14.0);
        AnchorPane.setTopAnchor(table, 50.0);
        AnchorPane.setBottomAnchor(table, 15.0);
        AnchorPane.setRightAnchor(table, 14.0);

        AnchorPane.setTopAnchor(txtFilter, 10.0);
        AnchorPane.setRightAnchor(txtFilter, 14.0);

        root.getChildren().addAll(anchorPane);
        Scene scene = new Scene(root, 600, 550);

        primaryStage.setTitle("FontAwesome Sample List");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

