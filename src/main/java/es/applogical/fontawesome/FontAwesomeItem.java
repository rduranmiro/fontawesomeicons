package es.applogical.fontawesome;


import javafx.beans.property.SimpleStringProperty;
import org.kordamp.ikonli.javafx.FontIcon;

/**
 * @author RamonDuran
 */
public class FontAwesomeItem {
    private final SimpleStringProperty iconName;
    private final SimpleStringProperty description;
    private final FontIcon icon;

    public FontAwesomeItem(String iconName, FontIcon icon, String description) {
        this.iconName = new SimpleStringProperty(iconName);
        this.icon = icon;
        this.description = new SimpleStringProperty(description);
    }

    public String getIconName() {
        return iconName.get();
    }

    public FontIcon getIcon() {
        return icon;
    }

    public String getDescription() {
        return description.get();
    }
}
